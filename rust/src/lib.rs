use serde::{de::DeserializeOwned, Deserialize};

#[derive(Debug)]
pub enum Error {
    HTTP(ureq::Error),
    Parsing(String),
}
//impl std::error::Error for Error{}

fn get_and_deserialize<T: DeserializeOwned>(url: &str) -> Result<T, Error> {
    ureq::get(url)
        .call()
        .map_err(|e| Error::HTTP(e))?
        .into_json()
        .map_err(|e| Error::Parsing(format!("{:?}", e)))
}

pub struct TeclaInstitution {
    base_url: String,
    name: String,
    license: String,
}

impl TeclaInstitution {
    pub fn get_areas(&self) -> Result<Vec<tecla_common::Area>, Error> {
        get_and_deserialize(&format!("{}/areas", self.base_url))
    }

    pub fn get_institution_name(&self) -> &str {
        self.name.as_str()
    }

    pub fn get_courses_with_area(&self, area_id: &str) -> Result<Vec<tecla_common::Course>, Error> {
        get_and_deserialize(&format!("{}/{}/courses", self.base_url, area_id))
    }

    pub fn get_curricula_for_course(
        &self,
        course_id: &str,
    ) -> Result<Vec<tecla_common::Curriculum>, Error> {
        get_and_deserialize(&format!("{}/course/{}/curricula", self.base_url, course_id))
    }

    pub fn get_teachings_for_curriculum(
        &self,
        curriculum_id: &str,
        year: u8,
    ) -> Result<Vec<tecla_common::Teaching>, Error> {
        get_and_deserialize(&format!(
            "{}/curriculum/{}/teachings/{}",
            self.base_url, curriculum_id, year
        ))
    }

    pub fn get_timetable_with_teachings(
        &self,
        curriculum_id: &str,
        teachings: &[&str],
        year: u8,
    ) -> Result<Vec<tecla_common::Lesson>, Error> {
        let mut req = ureq::get(&format!(
            "{}/curriculum/{}/timetable/{}",
            self.base_url, curriculum_id, year
        ));
        for teaching in teachings {
            req = req.query("teaching", teaching);
        }
        req.call()
            .map_err(|e| Error::HTTP(e))?
            .into_json()
            .map_err(|e| Error::Parsing(format!("{:?}", e)))
    }

    pub fn get_data_license(&self) -> &str {
        self.license.as_str()
    }
}
pub struct TeclaClient {
    base_url: String,
}
impl TeclaClient {
    pub fn new(url: &str) -> Self {
        Self {
            base_url: url.to_string(),
        }
    }
    pub fn get_learning_institutions(&self) -> Result<Vec<TeclaInstitution>, Error> {
        #[derive(Deserialize)]
        struct TeclaInstitutionData {
            pub id: String,
            pub name: String,
            pub license: String,
        }

        get_and_deserialize(&format!("{}/unis", self.base_url)).map(
            |x: Vec<TeclaInstitutionData>| {
                x.into_iter()
                    .map(|x| TeclaInstitution {
                        base_url: format!("{}/{}", self.base_url, x.id),
                        name: x.name.clone(),
                        license: x.license.clone(),
                    })
                    .collect()
            },
        )
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
