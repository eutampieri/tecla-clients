const fetch = (...args) => import('node-fetch').then(({ default: fetch }) => fetch(...args));


class Tecla {
    constructor(baseUrl) {
        this.baseUrl = baseUrl
    }
    async getLearningInstitutions() {
        let unis = await fetch(this.baseUrl + "/unis").then(r => r.json());
        let baseUrl = this.baseUrl;
        return unis.map(function (el) {
            return new TeclaUniversity(
                el.name,
                el.license,
                baseUrl + "/unis/" + el.id,
                el.id
            );
        });
    }
}

class TeclaUniversity {
    constructor(name, license, url, id) {
        this.name = name;
        this.license = license;
        this.url = url;
        this.id = id
    }

    async getAreas() {
        return await fetch(this.url + "/areas").then(r => r.json());
    }
    async getCoursesWithArea(areaId) {
        return await fetch(this.url + "/areas/" + areaId + "/courses").then(r => r.json());
    }
    async getCurriculaForCourse(courseId) {
        return await fetch(this.url + "/courses/" + courseId + "/curricula").then(r => r.json());
    }
    async getTeachingsForCurriculum(curriculumId, year) {
        return await fetch(this.url + "/curricula/" + curriculumId + "/teachings/" + year.toString()).then(r => r.json());
    }
    async getTimetableWithTeaching(curriculumId, teachings, year) {
        let qryStr = "?";
        for (let i = 0; i < teachings.length; i++) {
            qryStr = qryStr + "teachings=" + encodeURIComponent(teachings[i]) + "&";
        }
        qryStr = qryStr.slice(0, -1)
        return await fetch(this.url + "/curricula/" + curriculumId + "/timetable/" + year.toString() + qryStr).then(r => r.json());
    }
}

module.exports.Tecla = Tecla;
