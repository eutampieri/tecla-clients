package tecla

import "time"

type Area struct {
	Id   string `json:"id"`
	Name string `json:"name"`
}

type Course struct {
	Id              string `json:"id"`
	Name            string `json:"name"`
	AreaId          string `json:"area_id"`
	DurationInYears uint8  `json:"duration_in_years"`
}

type Curriculum struct {
	Id   string `json:"id"`
	Name string `json:"name"`
}

type Lesson struct {
	Start          time.Time `json:"start"`
	End            time.Time `json:"end"`
	Teacher        *Teacher  `json:"teacher"`
	Venue          *string   `json:"venue"`
	Teaching       Teaching  `json:"teaching"`
	OnlineClassUrl *string   `json:"online_class_url"`
}

type Teacher struct {
	Name  string  `json:"name"`
	Email *string `json:"email"`
}

type Teaching struct {
	Id   string `json:"id"`
	Name string `json:"name"`
}

type Client struct {
	BaseURL string
}

func (c Client) GetAvailableInstitutions() []Institution {
	return []Institution{}
}

type Institution struct {
	Name    string `json:"name"`
	License string `json:"license"`
	Url     string `json:"id"` // Please prepend BaseUrl + "/" to get this field
}

func (i Institution) GetAreas() []Area {
	return []Area{}
}

func (i Institution) GetCoursesWithArea(areaId string) []Course {
	return []Course{}
}

func (i Institution) GetCurriculaForCourse(courseId string) []Curriculum {
	return []Curriculum{}
}

func (i Institution) GetTeachingsForCurriculum(curriculumId string, year uint8) []Teaching {
	return []Teaching{}
}

func (i Institution) GetTimetableWithTeaching(curriculumId string, year uint8, teachings []string) []Lesson {
	return []Lesson{}
}
