import XCTest
@testable import TeclaClient

final class TeclaClientTests: XCTestCase {
    func testExample() throws {
        let client = TeclaClient(url: "https://tecla.eutampieri.eu")
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        let institution =  try client.institutions[0]
        print("\(institution.name)")
        let area = try institution.areas[0]
        print("\(area)")
        let course = try institution.getCoursesWithArea(areaId: area.id)[0]
        print("\(course)")
        let curriculum = try institution.getCurriculaForCourse(courseId: course.id)[0]
        print("\(curriculum)")
        let teachings = try institution.getTeachingsForCurriculum(curriculumId: curriculum.id, year: 1)
        print("\(teachings)")
        let timetable = try institution.getTimetableWithTeaching(curriculumId: curriculum.id, year: 1, teachings: teachings)
        print("\(timetable)")
    }
}
