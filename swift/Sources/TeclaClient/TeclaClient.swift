import Foundation

public final class TeclaClient {
    var baseUrl: String

    public init(url: String) {
        self.baseUrl = url
    }

    public var institutions: [Institution] {
        get throws {
            struct RawInstitution: Codable {
                var name: String
                var id: String
                var license: String
                
                func toInstitution(baseUrl: String) -> Institution {
                    return Institution(baseUrl: baseUrl, id: self.id, name: self.name, license: self.license)
                }
            }
            let data = try Data(contentsOf: URL(string: "\(self.baseUrl)/unis")!)
            let parsed: [RawInstitution] = try! JSONDecoder().decode([RawInstitution].self, from: data) as [RawInstitution]

            return parsed.map {el in
                el.toInstitution(baseUrl: self.baseUrl)
            }
        }
    }
}

public final class Institution {
    var _name: String
    var _license: String
    var url: String

    public init(baseUrl: String, id: String, name: String, license: String) {
        self.url = "\(baseUrl)/\(id)"
        self._name = name
        self._license = license
    }

    public var name: String {
        get { return _name }
    }
    public var license: String {
        get { return _license }
    }

    public var areas: [Area] {
        get throws {
            let data = try Data(contentsOf: URL(string: "\(self.url)/areas")!)
            return try! JSONDecoder().decode([Area].self, from: data) as [Area]
        }
    }

    public func getCoursesWithArea(areaId: String) throws -> [Course] {
        let data = try Data(contentsOf: URL(string: "\(self.url)/\(areaId)/courses")!)
        return try! JSONDecoder().decode([Course].self, from: data) as [Course]
    }

    public func getCurriculaForCourse(courseId: String) throws -> [Curriculum] {
        let data = try Data(contentsOf: URL(string: "\(self.url)/course/\(courseId)/curricula")!)
        return try! JSONDecoder().decode([Curriculum].self, from: data) as [Curriculum]
    }

    public func getTeachingsForCurriculum(curriculumId: String, year: UInt8) throws -> [Teaching] {
        let data = try Data(contentsOf: URL(string: "\(self.url)/curriculum/\(curriculumId.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? "")/teachings/\(year)")!)
        return try! JSONDecoder().decode([Teaching].self, from: data) as [Teaching]
    }

    public func getTimetableWithTeaching(curriculumId: String, year: UInt8, teachings: [Teaching]) throws -> [Lesson] {
        let queryString = teachings.map {t in "teachings=\(t.id)"}.joined(separator: "&")
        let data = try Data(contentsOf: URL(string: "\(self.url)/curriculum/\((curriculumId.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? ""))/timetable/\(year)?\(queryString)")!)
        return try! JSONDecoder().decode([Lesson].self, from: data) as [Lesson]    }
    
}
