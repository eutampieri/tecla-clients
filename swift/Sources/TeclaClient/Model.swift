import Foundation

public struct Area: Codable {
    var id: String
    var name: String
}

public struct Course: Codable {
    var id: String
    var name: String
    var areaId: String
    var durationInYears: UInt8
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case areaId = "area_id"
        case durationInYears = "duration_in_years"
    }
}

public struct Curriculum: Codable {
    var id: String
    var name: String
}

public struct Lesson: Codable {
    var start: Date
    var end: Date
    var teacher: Teacher?
    var venue: String?
    var teaching: Teaching
    var onlineClassUrl: String?
    
    enum CodingKeys: String, CodingKey {
        case start = "start"
        case end = "end"
        case teacher = "teacher"
        case venue = "venue"
        case teaching = "teaching"
        case onlineClassUrl = "online_class_url"
    }
}

public struct Teacher: Codable {
    var name: String
    var email: String?
}

public struct Teaching: Codable {
    var id: String
    var name: String
}
